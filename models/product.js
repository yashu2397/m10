/** 
*  Product model
*  Describes the characteristics of each attribute in a product resource.
*
*  @author Srikar Patle
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const ProductsSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  Product_Name: {
    type: String,
    required: true,
    unique: true
  },
  Product_Type: {
    type: String,
    required: true,
    default: 'Please select product Type'
  },
  Product_Description: { 
    type: String,
    required: true,
    default: ''
  },
  Product_Price: {
    type: Number,
    required: true,
    default: 0.0
  },
  Product_ManfactureDate: {
    type: String,
    required: false,
    default: ''
  },
  Product_ExpiryDate: {
    type: String,
    required: true,
    default: ''
  },
  Product_Status: {
    type: String,
    required: true,
    default: 'Available'
  }
})
module.exports = mongoose.model('Product', ProductsSchema)