/**
 * @index.js - manages all routing
 *
 * router.get when assigning to a single request
 * router.use when deferring to a controller
 *
 * @requires express
 */

const express = require("express");
const LOG = require("../utils/logger.js");


LOG.debug("START routing");
const router = express.Router();

// Manage top-level request first

router.get("/", function (req, res) {
  res.render("index.ejs")
})

// Defer path requests to a particular controller

router.use('/customer', require('../controllers/Customer.js'))
router.use('/order', require('../controllers/Order.js'))
router.use('/orderLineItems', require('../controllers/orderLineItems.js'))
router.use('/product', require('../controllers/product.js'))
//router.use('/puppy', require('../controllers/puppy.js'))

// router.use("/customer", require("../controllers/Customer.js"));
// router.use("/product", require("../controllers/product.js"));
// router.use("/orders", require("../controllers/Order.js"));
// router.use("/OrderLineItems", require("../controllers/orderLineItems.js"));

LOG.debug("END routing");
module.exports = router;
